import collections
import json


def get_locations_by_id(patient_id: str, path: str = 'areas.json') -> dict:
    """
    get id of patient and return his locations map in dictionary object from json
    :param patient_id: the patient id that we wanna get his locations map
    :param path: the file name of the json
    """

    with open(f'./json/{path}') as file:
        data = json.load(file)
        # print(data)
        # print(data[str(patient_id)])
    try:
        return data[str(patient_id)]
    except KeyError:
        return []


def update(orig_dict: dict, new_dict: dict):
    """
    util function to update json  dictionary nested in dictionary
    :param orig_dict: the original dictionary -the all json file without our new item
    :param new_dict:  the new dictionary item that we wanna insert

    """
    print(orig_dict)
    for key, val in new_dict.items():
        print(0)
        if isinstance(val, collections.Mapping):
            print(1)
            tmp = update(orig_dict.get(key, {}), dict(val))
            orig_dict[key] = tmp
        # elif isinstance(val, list):
        #     orig_dict[key] = (orig_dict.get(key, []) +val)
        else:
            print(2)
            orig_dict[key] = new_dict[key]
    return orig_dict


def update_json(dict_to_update: dict, path: str = 'areas.json'):
    """
    update json file a with new data
    :param dict_to_update: the dictionary that we wanna add to the json file
    :param path: the path name of the file that we wanna update
    """
    with open(f'./json/{path}') as file:
        data = json.load(file)
        # update to json object with the new data
    update(data, dict_to_update)
    # write it back to the json file
    with open(f'./json/{path}', 'w') as file:
        json.dump(data, file)


def delete_key_from_json(key:str,path:str='areas.json'):
    with open(f'./json/{path}') as file:
        data = json.load(file)
        data.pop(key)

    # for item in data.keys():
    #     print(item)
    #     try:
    #         data[item].pop(key)
    #     except KeyError:
    #         print('except')
    #         pass

    with open(f'./json/{path}', 'w') as file:
        json.dump(data, file)


