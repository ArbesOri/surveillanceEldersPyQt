-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 22, 2018 at 12:36 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rescue`
--
CREATE DATABASE IF NOT EXISTS `rescue` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `rescue`;

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

CREATE TABLE `areas` (
  `id` int(11) NOT NULL,
  `area_name` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`id`, `area_name`) VALUES
(1, 'Kitchen'),
(2, 'Salon'),
(3, 'Room 1'),
(4, ' Room 2'),
(5, 'Corridor');

-- --------------------------------------------------------

--
-- Table structure for table `cameras`
--

CREATE TABLE `cameras` (
  `id` int(11) NOT NULL,
  `ip` text NOT NULL,
  `patient_id` int(11) NOT NULL,
  `area_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cameras`
--

INSERT INTO `cameras` (`id`, `ip`, `patient_id`, `area_name`) VALUES
(1, '212.0.0.1', 222343535, 'bedroom'),
(3, '127.0.0.1', 200494474, 'salon');

-- --------------------------------------------------------

--
-- Table structure for table `diseases`
--

CREATE TABLE `diseases` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `medicines` int(30) NOT NULL,
  `medical_guideline` text NOT NULL,
  `symptoms` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `diseases`
--

INSERT INTO `diseases` (`id`, `name`, `medicines`, `medical_guideline`, `symptoms`) VALUES
(1, 'headache', 2, '', 'headache');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `event` int(11) NOT NULL,
  `date` text NOT NULL,
  `time` text NOT NULL,
  `count` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `patient_id`, `event`, `date`, `time`, `count`) VALUES
(1247, 135081248, 1, '08/08/2018', '13:53', '1'),
(1248, 135081248, 1, '08/08/2018', '13:53', '2'),
(1249, 135081248, 1, '08/08/2018', '13:53', '4'),
(1250, 135081248, 1, '08/08/2018', '13:53', '2'),
(1251, 135081248, 1, '08/08/2018', '13:53', '3'),
(1252, 135081248, 1, '08/08/2018', '13:54', '1'),
(1253, 135081248, 1, '08/08/2018', '13:54', '1'),
(1254, 135081248, 1, '08/08/2018', '13:54', '2'),
(1255, 135081248, 1, '08/08/2018', '13:54', '2'),
(1256, 135081248, 1, '08/08/2018', '13:54', '2'),
(1257, 135081248, 1, '08/08/2018', '13:54', '1'),
(1258, 135081248, 1, '08/08/2018', '13:54', '1'),
(1259, 135081248, 1, '08/08/2018', '13:54', '1'),
(1260, 135081248, 1, '08/08/2018', '13:54', '1'),
(1261, 135081248, 1, '08/08/2018', '13:54', '1'),
(1262, 135081248, 1, '08/08/2018', '13:55', '1'),
(1263, 135081248, 1, '08/08/2018', '13:55', '1'),
(1264, 135081248, 1, '08/08/2018', '13:55', '1'),
(1265, 135081248, 1, '08/08/2018', '13:55', '1'),
(1266, 135081248, 1, '08/08/2018', '13:55', '1'),
(1267, 135081248, 1, '08/08/2018', '13:55', '1'),
(1268, 135081248, 1, '08/08/2018', '13:55', '1'),
(1269, 135081248, 1, '08/08/2018', '13:55', '1'),
(1270, 135081248, 1, '08/08/2018', '13:55', '1'),
(1271, 135081248, 1, '08/08/2018', '13:55', '1'),
(1272, 135081248, 1, '08/08/2018', '13:55', '1'),
(1273, 135081248, 1, '08/08/2018', '13:55', '1'),
(1274, 135081248, 1, '08/08/2018', '13:55', '1'),
(1275, 135081248, 1, '08/08/2018', '13:55', '1'),
(1276, 135081248, 1, '08/08/2018', '13:56', '1'),
(1277, 135081248, 1, '08/08/2018', '13:56', '1'),
(1278, 135081248, 1, '08/08/2018', '13:56', '1'),
(1279, 135081248, 1, '08/08/2018', '13:56', '1'),
(1280, 135081248, 1, '08/08/2018', '13:56', '1'),
(1281, 135081248, 1, '08/08/2018', '13:56', '1'),
(1282, 135081248, 1, '08/08/2018', '13:56', '1'),
(1283, 135081248, 1, '08/08/2018', '13:56', '2'),
(1284, 135081248, 1, '08/08/2018', '13:56', '2'),
(1285, 135081248, 1, '08/08/2018', '13:56', '1'),
(1286, 135081248, 1, '08/08/2018', '13:56', '2'),
(1287, 135081248, 1, '08/08/2018', '13:56', '1'),
(1288, 135081248, 1, '08/08/2018', '13:56', '1'),
(1289, 135081248, 1, '08/08/2018', '13:56', '2'),
(1290, 135081248, 1, '08/08/2018', '13:56', '1'),
(1291, 135081248, 1, '08/08/2018', '13:56', '2'),
(1292, 135081248, 1, '08/08/2018', '13:56', '2'),
(1293, 135081248, 1, '08/08/2018', '13:56', '1'),
(1294, 135081248, 1, '08/08/2018', '13:56', '1'),
(1295, 135081248, 1, '08/08/2018', '13:56', '1'),
(1296, 135081248, 1, '08/08/2018', '13:56', '1'),
(1297, 135081248, 1, '08/08/2018', '13:56', '1'),
(1298, 135081248, 1, '08/08/2018', '13:56', '1'),
(1299, 200494474, 1, '09/08/2018', '17:11', '1'),
(1300, 200494474, 1, '09/08/2018', '17:11', '2'),
(1301, 200494474, 1, '09/08/2018', '17:11', '2'),
(1302, 200494474, 1, '09/08/2018', '17:11', '2'),
(1303, 200494474, 1, '09/08/2018', '17:11', '1'),
(1304, 200494474, 1, '09/08/2018', '17:11', '1'),
(1305, 200494474, 1, '09/08/2018', '17:11', '1'),
(1306, 200494474, 1, '09/08/2018', '17:11', '1'),
(1307, 200494474, 1, '09/08/2018', '17:11', '2'),
(1308, 200494474, 1, '09/08/2018', '17:11', '1'),
(1309, 200494474, 1, '09/08/2018', '17:11', '1'),
(1310, 200494474, 1, '09/08/2018', '17:11', '1'),
(1311, 200494474, 1, '09/08/2018', '17:11', '1'),
(1312, 200494474, 1, '09/08/2018', '17:11', '1'),
(1313, 200494474, 1, '09/08/2018', '17:11', '1'),
(1314, 200494474, 1, '09/08/2018', '17:11', '1'),
(1315, 200494474, 1, '09/08/2018', '17:11', '2'),
(1316, 200494474, 1, '09/08/2018', '17:11', '1'),
(1317, 200494474, 1, '09/08/2018', '17:11', '1'),
(1318, 200494474, 1, '09/08/2018', '17:11', '1'),
(1319, 200494474, 1, '09/08/2018', '17:11', '1'),
(1320, 200494474, 1, '09/08/2018', '17:11', '1'),
(1321, 200494474, 1, '09/08/2018', '17:11', '1'),
(1322, 200494474, 1, '09/08/2018', '17:11', '1'),
(1323, 200494474, 1, '09/08/2018', '17:11', '1'),
(1324, 200494474, 1, '09/08/2018', '17:11', '1'),
(1325, 200494474, 1, '09/08/2018', '17:11', '1'),
(1326, 200494474, 1, '09/08/2018', '17:11', '1'),
(1327, 200494474, 1, '09/08/2018', '17:11', '1'),
(1328, 200494474, 1, '09/08/2018', '17:11', '1'),
(1329, 200494474, 1, '09/08/2018', '17:11', '1'),
(1330, 200494474, 1, '09/08/2018', '17:11', '1'),
(1331, 200494474, 1, '09/08/2018', '17:11', '1'),
(1332, 200494474, 1, '09/08/2018', '17:11', '1'),
(1333, 200494474, 1, '09/08/2018', '17:11', '1'),
(1334, 200494474, 1, '09/08/2018', '17:11', '1'),
(1335, 200494474, 1, '09/08/2018', '17:11', '1'),
(1336, 200494474, 1, '09/08/2018', '17:11', '1'),
(1337, 200494474, 1, '09/08/2018', '17:12', '1'),
(1338, 200494474, 1, '09/08/2018', '17:12', '1'),
(1339, 200494474, 1, '09/08/2018', '17:12', '1'),
(1340, 200494474, 1, '09/08/2018', '17:12', '1'),
(1341, 200494474, 1, '09/08/2018', '17:12', '2'),
(1342, 200494474, 1, '09/08/2018', '17:12', '1'),
(1343, 200494474, 1, '09/08/2018', '17:12', '1'),
(1344, 200494474, 1, '09/08/2018', '17:12', '1'),
(1345, 200494474, 1, '09/08/2018', '17:12', '1'),
(1346, 200494474, 1, '09/08/2018', '17:12', '1'),
(1347, 200494474, 1, '09/08/2018', '17:12', '1'),
(1348, 200494474, 1, '09/08/2018', '17:12', '1'),
(1349, 200494474, 1, '09/08/2018', '17:12', '1'),
(1350, 200494474, 1, '09/08/2018', '17:12', '1'),
(1351, 200494474, 1, '09/08/2018', '17:12', '1'),
(1352, 200494474, 1, '09/08/2018', '17:12', '1'),
(1353, 200494474, 1, '09/08/2018', '17:12', '1'),
(1354, 200494474, 1, '09/08/2018', '17:12', '1'),
(1355, 200494474, 1, '09/08/2018', '17:12', '1'),
(1356, 200494474, 1, '09/08/2018', '17:12', '1'),
(1357, 200494474, 1, '09/08/2018', '17:12', '1');

-- --------------------------------------------------------

--
-- Table structure for table `events_name`
--

CREATE TABLE `events_name` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events_name`
--

INSERT INTO `events_name` (`id`, `name`) VALUES
(1, 'Get out from the bed'),
(2, 'Get out from the room');

-- --------------------------------------------------------

--
-- Table structure for table `medicines`
--

CREATE TABLE `medicines` (
  `id` int(11) NOT NULL,
  `manufacturer` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `active_ingredient` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medicines`
--

INSERT INTO `medicines` (`id`, `manufacturer`, `active_ingredient`, `name`) VALUES
(1, 'Teva', 'Peractamol', 'Acamol'),
(2, 'teva', 'paracetamol', 'acamol');

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `id` int(11) NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `phone` text NOT NULL,
  `cellphone` text NOT NULL,
  `address` text NOT NULL,
  `mail` text NOT NULL,
  `diseases` int(11) NOT NULL,
  `medicines` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`id`, `first_name`, `last_name`, `phone`, `cellphone`, `address`, `mail`, `diseases`, `medicines`) VALUES
(135081248, 'arik', 'momo', '021295714', '0501297291', 'arav agan', 'arikm@gmail.com', 1, 1),
(200494474, 'ori', 'arbes', '0504162028', '021239576', 'hazav', 'arbeson@gail.com', 1, 1),
(222343535, 'moshe', 'levi', '0252922841', '0506298214', 'Herzel', 'moshelevi@gmail.com', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cameras`
--
ALTER TABLE `cameras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patient_id` (`patient_id`);

--
-- Indexes for table `diseases`
--
ALTER TABLE `diseases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `medicines` (`medicines`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event` (`event`),
  ADD KEY `patient_id` (`patient_id`);

--
-- Indexes for table `events_name`
--
ALTER TABLE `events_name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medicines`
--
ALTER TABLE `medicines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diseases` (`diseases`),
  ADD KEY `medicines` (`medicines`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cameras`
--
ALTER TABLE `cameras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `diseases`
--
ALTER TABLE `diseases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1358;

--
-- AUTO_INCREMENT for table `events_name`
--
ALTER TABLE `events_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `medicines`
--
ALTER TABLE `medicines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cameras`
--
ALTER TABLE `cameras`
  ADD CONSTRAINT `cameras_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`);

--
-- Constraints for table `diseases`
--
ALTER TABLE `diseases`
  ADD CONSTRAINT `diseases_ibfk_1` FOREIGN KEY (`medicines`) REFERENCES `medicines` (`id`);

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_ibfk_1` FOREIGN KEY (`event`) REFERENCES `events_name` (`id`),
  ADD CONSTRAINT `events_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`);

--
-- Constraints for table `patients`
--
ALTER TABLE `patients`
  ADD CONSTRAINT `patients_ibfk_1` FOREIGN KEY (`diseases`) REFERENCES `diseases` (`id`),
  ADD CONSTRAINT `patients_ibfk_2` FOREIGN KEY (`medicines`) REFERENCES `medicines` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
