import datetime
from time import sleep

import cv2
import numpy as np
from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import QTimer, QUrl
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWidgets import QWidget, QMainWindow, QInputDialog, QTableWidgetItem, QFileDialog
from PyQt5.uic import loadUi

from db_functions import Areas, Patients, Events, db
from json_functions import *

personSize = 2000
frontface_classifier = cv2.CascadeClassifier('./haarcascade/haarcascade_frontalface_default.xml')
body_classifier = cv2.CascadeClassifier('./haarcascade/haarcascade_fullbody.xml')
upperbody_classifier = cv2.CascadeClassifier('./haarcascade/haarcascade_upperbody.xml')
human_classifier = cv2.CascadeClassifier('./haarcascade/human.xml')

def get_patient_name_to_list():
    patients = Patients.query.all()
    return [f"{value.first_name} {value.last_name} - {value.id}" for value in patients]


def get_areas_to_list():
    areas = Areas.query.all()
    return [f"{value.area_name}" for value in areas]


def get_only_id(full_name: str):
    index = full_name.index('-')
    print(full_name[index + 2:])
    return full_name[index + 2:]


class SurveillanceUiClass(QMainWindow):
    def __init__(self, parent=None):

        QWidget.__init__(self, parent)
        super(SurveillanceUiClass, self).__init__()
        loadUi('Gui.ui', self)
        self.frame_counter = 0
        self.video_path = 'priza-2.mp4'
        self.capture = cv2.VideoCapture(self.video_path)
        web = QWebEngineView()
        web.load(QUrl("http://127.0.0.1/phpmyadmin/db_structure.php?server=1&db=rescue"))
        self.PatientComboBox.clear()
        self.classifier=upperbody_classifier
        self.PatientComboBox.addItems(get_patient_name_to_list())
        self.PatientComboBox.currentIndexChanged.connect(self.update_patient_combobox)

        selected_patient, ok = QInputDialog.getItem(self, "Please Choose Patient Name",
                                                    "Areas List", get_patient_name_to_list(), 0, False)

        self.current_patient_id = get_only_id(selected_patient)

        index = self.PatientComboBox.findText(selected_patient, QtCore.Qt.MatchFixedString)
        if index >= 0:
            self.PatientComboBox.setCurrentIndex(index)
        self.StartVideoButton.clicked.connect(self.start_webcam)
        self.StopVideoButton.clicked.connect(self.stop_webcam)
        self.ClearArea.clicked.connect(self.clear_area)
        self.changeVideoFile.clicked.connect(self.changeFileFunc)
        self.area_counter_dict = {value: 0 for value in get_locations_by_id(self.current_patient_id)}
        self.tabWidget.setCurrentIndex(0)
        self.start_webcam()
        self.recPt = []
        self.mainScreen.mousePressEvent = self.PressPixelSelect
        self.mainScreen.mouseReleaseEvent = self.ReleasePixelSelect
        self.WebLayout.addWidget(web)
        self.person_detected_list=[]
        self.person_detected_list_extend=[]
        # self.RecentActivitiesTable.setItem(0,  )

        with open('style.css', "r") as fh:
            self.styleSheet = fh.read()

        self.setStyleSheet(self.styleSheet)
        self.allContour = []

    def person_detector(self,img, accurate: float = 1.03, sensitivity: int = 1):
        """

        create the required classifier and detects the object,  return list of cord lists.
        :param accurate: the accurate level (must to be more then 1 ,smaller value is the best accurate )
        :param sensitivity: the sensitivity level  (smaller value is more sensitivity)

        """

        # Convert image to grayscale
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


        self.person_detected_list = list(self.classifier.detectMultiScale(gray, accurate, sensitivity))

        # self.person_detected_list_extend = self.classifier.detectMultiScale3(gray, accurate, sensitivity,
        #                                                                      outputRejectLevels=True)
        # print(self.person_detected_list_extend[2])
        print(f"fffff {self.person_detected_list}")


        # self.img = cv2.resize(self.img, (0, 0), fx=4, fy=4)

    def changeFileFunc(self):
        filepath, filter = QFileDialog.getOpenFileName(parent=self, caption='Open file', directory='.',
                                                             filter='mp4 (*.mp4)')

        self.stop_webcam()
        index=filepath[::-1].index('/')
        index=(len(filepath)-index)
        filename=filepath[index:]
        # if filename!='webcam.mp4':
        #     self.capture = cv2.VideoCapture(filepath)
        # else:
        #     self.capture = cv2.VideoCapture(0)
        _, self.rawImage = self.capture.read()
        self.rawImage = cv2.resize(self.rawImage,
                                   (self.mainScreen.frameGeometry().width(),
                                    self.mainScreen.frameGeometry().height()))
        self.firstFrame = cv2.cvtColor(self.rawImage, cv2.COLOR_BGR2GRAY)
        self.capture = cv2.VideoCapture(filepath)
        self.start_webcam()

    def start_webcam(self, camera=0):




        self.capture.set(cv2.CAP_PROP_FRAME_HEIGHT, self.mainScreen.frameGeometry().height())
        self.capture.set(cv2.CAP_PROP_FRAME_WIDTH, self.mainScreen.frameGeometry().width())
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_frame)
        self.timer.start(5)
        _, self.rawImage = self.capture.read()
        self.rawImage = cv2.resize(self.rawImage,
                                   (self.mainScreen.frameGeometry().width(),
                                    self.mainScreen.frameGeometry().height()))
        self.firstFrame = cv2.cvtColor(self.rawImage, cv2.COLOR_BGR2GRAY)

    def update_frame(self):
        ret, self.image = self.capture.read()
        # self.image = cv2.flip(self.image, 1)
        # sleep(0.02)
        self.display(self.image)

    def stop_webcam(self):

        self.timer.stop()

    def display(self, img, window=1):

        qformat = QImage.Format_Indexed8
        img = cv2.resize(img,
                         (self.mainScreen.frameGeometry().width(),
                          self.mainScreen.frameGeometry().height()))
        if len(img.shape) == 3:
            if img.shape[2] == 4:
                qformat = QImage.Format_RGBA8888
            else:
                qformat = QImage.Format_RGB888
        self.updateContours(image=img)
        self.draw_detections(img, self.contours, 2)
        current_id_areas = get_locations_by_id(self.current_patient_id)
        self.area_entered_count()
        self.draw_areas(img, current_id_areas, 2)
        self.update_events()
        # self.person_detector(img)
        # self.draw_detections(img, self.person_detected_list,5,(255,255,255))
        self.update_counter_areas()
        outImage = QImage(img, img.shape[1], img.shape[0], img.strides[0], qformat)
        outImage = outImage.rgbSwapped()

        if window == 1:
            self.mainScreen.setPixmap(QPixmap.fromImage(outImage))
            self.mainScreen.setScaledContents(True)

    def area_entered_count(self):
        current_id_areas = get_locations_by_id(self.current_patient_id)
        if current_id_areas:
            area_counter = {value: 0 for value in current_id_areas.keys()}
            for area_name in current_id_areas.keys():
                for contour in self.contours:
                    x1 = current_id_areas[area_name][0][0]
                    y1 = current_id_areas[area_name][0][1]
                    x2 = current_id_areas[area_name][1][0]
                    y2 = current_id_areas[area_name][1][1]
                    if contour[0] <= x2 and contour[0] + contour[2] >= x1:
                        # and not (contour[1] > y2 or contour[1] + contour[3] < y1):
                        area_counter[area_name] += 1
            return area_counter
        else:
            return False

    def update_counter_areas(self):

        self.RecentActivitiesTable.setRowCount(len(list(self.area_counter_dict.keys())))
        rowIndex = 0
        for area in self.area_counter_dict.keys():
            self.RecentActivitiesTable.setItem(rowIndex, 0, QTableWidgetItem(f"{area}"))
            self.RecentActivitiesTable.setItem(rowIndex, 1, QTableWidgetItem(f"{self.area_counter_dict[area]}"))
            rowIndex += 1

    def update_events(self):

        temp_dict = self.area_entered_count()
        if temp_dict:
            for key in temp_dict.keys():
                if temp_dict[key] != self.area_counter_dict[key]:
                    if temp_dict[key] > self.area_counter_dict[key]:
                        diff = temp_dict[key] - self.area_counter_dict[key]
                        self.RecentList.addItem(
                            f'{datetime.datetime.now().strftime("%d/%m/%Y %H:%M")} | {diff} Persons Entered into area {key}')
                        event_date = f'{datetime.datetime.now().strftime("%d/%m/%Y")}'
                        event_time = f'{datetime.datetime.now().strftime("%H:%M")}'

                        size=self.RecentList.count()

                        self.RecentList.setCurrentRow(int(size-1))
                        eventObj = Events(count=str(temp_dict[key]), date=event_date,
                                          time=event_time,
                                          patient_id=self.current_patient_id, event=1)

                        db.session.add(eventObj)
                        db.session.commit()
                        print("event person count has been updated")

                    else:
                        diff = self.area_counter_dict[key] - temp_dict[key]
                        self.RecentList.addItem(
                            f'{datetime.datetime.now().strftime("%d/%m/%Y %H:%M")} | {diff} Persons Left into area {key}')
                    self.area_counter_dict[key] = temp_dict[key]

    def update_patient_combobox(self):
        print("patient")
        changed_patient = str(self.PatientComboBox.currentText())
        self.current_patient_id = get_only_id(changed_patient)
        self.area_counter_dict = {value: 0 for value in get_locations_by_id(self.current_patient_id)}
        self.RecentList.clear()

    def clear_area(self):
        # temp_dict={}
        # temp_dict[self.current_patient_id]={}

        delete_key_from_json(self.current_patient_id)
        self.area_counter_dict = {value: 0 for value in get_locations_by_id(self.current_patient_id)}
        self.update_counter_areas()

    def updateContours(self, image):
        # keep looping infinitely until the thread is stopped
        global personSize
        total = datetime.datetime.now()
        img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # cv2.imshow('1',img)
        # cv2.imshow('2',self.firstFrame)
        frameDelta = cv2.absdiff(self.firstFrame, img)
        ret, self.thresh = cv2.threshold(frameDelta, 50, 255, cv2.THRESH_BINARY)
        kernel = np.ones((1, 1), np.uint8)
        self.thresh = cv2.erode(self.thresh, kernel, iterations=1)
        (_, allContours, _) = cv2.findContours(self.thresh.copy(), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        personContours = []
        self.allContours = []
        for c in allContours:
            # only look at contours larger than a certain size
            if cv2.contourArea(c) > personSize :
                personContours.append(cv2.boundingRect(c))
                self.allContours.append(c)
        self.contours = personContours
        self.contours=self.contours[:2]

    def draw_detections(self, img, rects, thickness=2, recColor=(0, 255, 0)):

        if rects:
            for x, y, w, h in rects:
                pad_w, pad_h = int(0.15 * w), int(0.05 * h)
                cv2.rectangle(img, (x, y), (x + w, y + h), recColor, thickness)

    def draw_areas(self, img, rects, thickness=2, recColor=(255, 0, 0)):
        if rects:
            for area_name in rects.keys():
                left = rects[area_name][0]
                right = rects[area_name][1]
                cv2.putText(img, area_name, (left[0], left[1] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 255))
                cv2.rectangle(img, tuple(left), tuple(right), recColor, thickness)

    def ReleasePixelSelect(self, event):
        x = event.x()
        y = event.y()
        self.recPt.append((x, y))

        self.update_frame()
        # items = ("Kitchen", "Children Room", "Parents Room", "Corridor")
        items = tuple(get_areas_to_list())
        item, ok = QInputDialog.getItem(self, "Please Choose Area Name",
                                        "Areas List", items, 0, False)

        new_area_dict = {}
        new_area_dict[self.current_patient_id] = {item: self.recPt}
        # load all the json file
        update_json(dict_to_update=new_area_dict, path=r'areas.json')
        self.area_counter_dict = {value: 0 for value in get_locations_by_id(self.current_patient_id)}

    def PressPixelSelect(self, event):

        x = event.x()
        y = event.y()
        self.recPt = [(x, y)]
