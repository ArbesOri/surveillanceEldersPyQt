from pathlib import Path

import MySQLdb as sql
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import session
from sqlalchemy.orm.sync import update

app = Flask(__name__)

CURRENT_FOLDER=Path().absolute().as_uri()
print(CURRENT_FOLDER)

# Connect to my existing mysql dataBase with Flask app
app.config['SQLALCHEMY_DATABASE_URI'] = r'mysql://root@localhost/rescue'
# app.config['SQLALCHEMY_DATABASE_URI'] = r'sqlite:///C:\Users\mylaptop30\PycharmProjects\PuzzleSoft-Projects\ComputerVision-Original\rescue_db_sqlite'

# initial Sql alchemy object
db = SQLAlchemy(app)


class Patients(db.Model):
    """
    Class of my Patient Table in 'rescue_db' database
    """

    def __init__(self, patient_id=None, first_name=None, last_name=None,
                 phone=None, cellphone=None, address=None, mail=None,
                 diseases=None, medicines=None):


        self.id = patient_id
        self.first_name = first_name
        self.last_name = last_name
        self.address = address
        self.phone = phone
        self.cellphone = cellphone
        self.mail = mail
        self.diseases = diseases
        self.medicines = medicines

    __tablename__ = 'patients'
    id = db.Column('id', db.Integer, primary_key=True)
    first_name = db.Column('first_name', db.String)
    last_name = db.Column('last_name', db.String)
    phone = db.Column('phone', db.String)
    cellphone = db.Column('cellphone', db.String, )
    address = db.Column('address', db.String)
    mail = db.Column('mail', db.String)
    diseases = db.Column('diseases', db.String)
    medicines = db.Column('medicines', db.String)


class Medicines(db.Model):
    """
    Class of my Medicines Table in 'rescue_db' database
    """

    def __init__(self, manufacturer=None, active_ingredient=None, name=None):
        self.manufacturer = manufacturer
        self.active_ingredient = active_ingredient
        self.name = name

    __tablename__ = 'medicines'
    id = db.Column('id', db.Integer, primary_key=True)
    manufacturer = db.Column('manufacturer', db.String)
    active_ingredient = db.Column('active_ingredient', db.String)
    name = db.Column('name', db.String)


class Diseases(db.Model):
    """
    Class of my Diseases Table in 'rescue_db' database
    """

    def __init__(self, name=None, medicines=None, symptoms=None):
        self.name = name
        self.medicines = medicines
        self.symptoms = symptoms

    __tablename__ = 'diseases'

    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column('name', db.String)
    medicines = db.Column('medicines', db.Integer)
    medical_guideline = db.Column('medical_guideline', db.String)
    symptoms = db.Column('symptoms', db.String)


class Cameras(db.Model):
    """
    Class of my Cameras Table in 'rescue_db' database
    """

    def __init__(self, ip=None, patient_id=None, area_name=None):
        self.ip = ip
        self.patient_id = patient_id
        self.area_name = area_name

    __tablename__ = 'cameras'
    id = db.Column('id', db.Integer, primary_key=True)
    ip = db.Column('ip', db.String)
    patient_id = db.Column('patient_id', db.Integer)
    area_name = db.Column('area_name', db.String)


class EventsName(db.Model):
    """
    Class of my Cameras Table in 'rescue_db' database
    """

    def __init__(self, id=None, name=None):
        self.id = id
        self.name = name

    __tablename__ = 'events_name'
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column('name', db.String)


class Events(db.Model):
    """
    Class of my Cameras Table in 'rescue_db' database
    """


    def __init__(self, patient_id=None, event=None, date=None, time=None,count=None, is_over=0):
        self.patient_id = patient_id
        self.event = event
        self.date = date
        self.time = time
        self.count = count
        # self.area_name = area_name



    __tablename__ = 'events'
    id = db.Column('id', db.Integer, primary_key=True)
    patient_id = db.Column('patient_id', db.String)
    event = db.Column('event', db.Integer)
    date = db.Column('date', db.String)
    time = db.Column('time', db.String)
    count = db.Column('count', db.String)
    # area_name = db.Column('area_name', db.Integer)



class Areas(db.Model):
    """
    Class of my Areas Table in 'rescue_db' database
    """
    def __init__(self, area_name):

        self.area_name = area_name

    __tablename__ = 'areas'
    id = db.Column('id', db.Integer, primary_key=True)
    area_name = db.Column('area_name', db.String)



# a=Areas('aa')
# a=[1,2,3,4]
# b=tuple(a)
# print(b)
# b=Areas.query.query.
# print([value.area_name for value in b])
# print(b)
# event_id = Patients.query.all()
# print(event_id[0].first_name)
# print(event_id)
# print(event_id)

# b = Events()
# print(b.query.filter_by(is_over=0,event=2).all())


# stmt = update(Events).where(Events.c.id==5).\
#         values(name='user #5')
# user=Events.query.filter_by(is_over="1").all()
# for u in user:
#     print(u)
# db.session.commit()

# def get_patient_list():
# eventObj = Events(comment=str(temp_dict[key]), is_over=True, date='08/08/2018',
#           time='02:53',
#           patient_id='135081248', event=1)
#
# print(eventObj)
# db.session.add(eventObj)
# db.session.commit()