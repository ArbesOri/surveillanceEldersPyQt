import datetime

import cv2
import numpy as np

from json_functions import *


frontface_classifier = cv2.CascadeClassifier('./haarcascade/haarcascade_frontalface_default.xml')
body_classifier = cv2.CascadeClassifier('./haarcascade/haarcascade_fullbody.xml')
upperbody_classifier = cv2.CascadeClassifier('./haarcascade/haarcascade_upperbody.xml')
human_classifier = cv2.CascadeClassifier('./haarcascade/human.xml')

def draw_areas2( img, rects, thickness=2, recColor=(255, 255, 255)):

    for left,right in rects:
        print(rects)
        print(left)
        print(tuple(left))
        print(right)
        print(tuple(right))
        # pad_w, pad_h = int(0.15 * w), int(0.05 * h)
        cv2.rectangle(img, tuple(left), tuple(right), recColor, thickness)


def person_detector(img, accurate: float = 1.1, sensitivity: int = 2):
    """

    create the required classifier and detects the object,  return list of cord lists.
    :param accurate: the accurate level (must to be more then 1 ,smaller value is the best accurate )
    :param sensitivity: the sensitivity level  (smaller value is more sensitivity)

    """
    # classifier=human_classifier
    # print(classifier)

    # Convert image to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cv2.imshow('gray',gray)
    print(gray)

    # person_detected_list = classifier.detectMultiScale(gray, accurate, sensitivity)

    # person_detected_list_extend = classifier.detectMultiScale3(gray, accurate, sensitivity,
    #                                                                      outputRejectLevels=True)
    # print(person_detected_list_extend[2])
    # print(person_detected_list)
    # self.img = cv2.resize(self.img, (0, 0), fx=4, fy=4)


capture=cv2.VideoCapture("79.180.66.4:60001/")

while True:

    _,img=capture.read()
    img = cv2.resize(img,(640,480))

    a = [[[348, 230], [579, 449]]]
    # draw_areas2(img,a,2)
    # person_detector(img)
    cv2.imshow('a',img)

    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break

print(datetime.datetime.now().strftime("%d/%m/%Y %H:%M"))